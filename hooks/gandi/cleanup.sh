#!/bin/bash

DOMAIN=$(echo $CERTBOT_DOMAIN | rev | cut -d '.' -f1,2 | rev)
SUBDOMAIN=${CERTBOT_DOMAIN/.$DOMAIN}

# Gandi livedns API KEY
export APIKEY="{{ gandi_api_key }}"

# Get the current Zone for the provided domain
        export CURRENT_ZONE_HREF=$(curl -s -H "X-Api-Key: $APIKEY" https://dns.api.gandi.net/api/v5/domains/$DOMAIN | jq -r '.domain_records_href')
        if [[ $? != 0 ]]; then
                echo "There was an error quering Gandi's LiveAPI to retrieve the domain's zone record href"
                exit 1
        fi

#echo "Entering cleanup script"i
#echo "Domain = $DOMAIN"
#echo "SubDomain = $SUBDOMAIN"
#echo "Email = $EMAIL"
#echo "CURRENT_ZONE_HREF = $CURRENT_ZONE_HREF"
#echo ''
#echo '----------------'
#echo "Certbot Output:"
#echo "CERTBOT_DOMAIN =  $CERTBOT_DOMAIN"
#echo "CERTBOT_VALIDATION = $CERTBOT_VALIDATION"

echo "Deleting TXT Record via Gandi LiveDNS for $CERTBOT_DOMAIN"
# Update the A reccord of the Dynamic Subdomain by PUTing on the current zone
curl -s -X DELETE -H "Content-Type: application/json" -H "X-Api-Key: $APIKEY" $CURRENT_ZONE_HREF/_acme-challenge.$SUBDOMAIN/TXT | jq -r '.message'

exit 0