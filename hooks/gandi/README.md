# Gandi hooks

Hooks to allow for certbot (LetsEncrypt) cert requests using the DNS authentication method.
These hooks will use your gandi API to generate DNS TXT records in your zone file for authentication.

## Preface

Rembember to add your Gandi API Key to your scripts, replacing the following line:

    export APIKEY="{{ gandi_api_key }}"

## Usage

Clone the repo into /opt, ensure auth.sh and cleanup.sh are executable, then execute the following, adjusting adding the required DNS names using the -d flag (minimum requirement is a single -d entry, but add as many more as you need).

    certbot certonly --non-interactive --agree-tos --manual-public-ip-logging-ok -m <email>@<tld>.com --manual --manual-auth-hook /opt/certbot/hooks/gandi/auth.sh --manual-cleanup-hook /opt/certbot/hooks/gandi/cleanup.sh -d <CommonName> -d <SubjectAlternateName 1> -d <SubjectAlternateName 2> --preferred-challenges dns --expand

Ensure you use the Full Path to the hooks (not relative paths), otherwise you will not be able to automatically renew your certs.
