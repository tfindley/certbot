#!/usr/bin/env python

import os
import sys
import tldextract
import requests

CERTBOT_DOMAIN = os.getenv('CERTBOT_DOMAIN')
CERTBOT_VALIDATION = os.getenv('CERTBOT_VALIDATION')
CERTBOT_REMAINING_CHALLENGES = os.getenv('CERTBOT_REMAINING_CHALLENGES')
CERTBOT_ALL_DOMAINS = os.getenv('CERTBOT_ALL_DOMAINS')

if not CERTBOT_DOMAIN:
    sys.exit('CERTBOT_DOMAIN variable is empty')

if not CERTBOT_VALIDATION:
    sys.exit('CERTBOT_VALIDATION variable is empty')

uri = (tldextract.extract(CERTBOT_DOMAIN))

if not uri.domain:
    sys.exit('uri.domain is empty - domain may be malformed')

if not uri.suffix:
    sys.exit('uri.domain is empty - domain may be malformed')

tld = uri.domain + "." + uri.suffix

GANDI_APIKEY = os.getenv('GANDI_APIKEY')
GANDI_APIURL = "https://dns.api.gandi.net/api/v5/domains/" + tld

HEADERS = {
    "X-Api-Key": GANDI_APIKEY,
    "Content-Type": "application/json",

}

if not uri.subdomain:
    PAYLOAD = "/_acme-challenge/TXT"
else:
    PAYLOAD = "/_acme-challenge." + uri.subdomain + "/TXT"

if not GANDI_APIKEY:
    sys.exit('GANDI_APIKEY variable is empty')

if not GANDI_APIURL:
    sys.exit('GANDI_APIURL variable is empty')

if not HEADERS:
    sys.exit('HEADERS is empty')

if not PAYLOAD:
    sys.exit('PAYLOAD is empty')

print( "" )
print( "        Domain variables")
print( "        ----------------")
print( "                  Suffix: " + uri.suffix)
print( "                  Domain: " + uri.domain)
print( "               Subdomain: " + uri.subdomain)
print( "" )
print( "        CertBot variables")
print( "        -----------------")
print( "                 Domain: " + CERTBOT_DOMAIN)
print( "             Validation: " + CERTBOT_VALIDATION)
print( "        Remaining Chals: " + CERTBOT_REMAINING_CHALLENGES)
print( "             All Domain: " + CERTBOT_ALL_DOMAINS)

req = requests.get(url = GANDI_APIURL, headers = HEADERS)
current_zone_url=(req.json()[u'domain_records_href'])

if not current_zone_url:
    sys.exit('current_zone_url variable is empty')

print( "" )
print( "        Gandi variables")
print( "        ---------------")
print( "                  API URL: " + GANDI_APIURL)
print( "        Current Zone HREF: " + current_zone_url)

delete = requests.delete(url = current_zone_url + PAYLOAD, headers = HEADERS )
print( "" )
print(delete.status_code)